
Drupal.behaviors.awesomengineColorPicker = function() {
  var updateColors = function(self) {
    var val = $(self).val();
    if (val.length != 4 && val.length != 7) {
      return;
    }
    if (val.indexOf('#') != 0) {
      return;
    }
    $(self).css('background-color', val);
  };
  $('.color-picker').keyup(function() {
    updateColors(this);
  });
  $('.color-picker').each(function() {
    updateColors(this);
  });
};